#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
import sys
import json
import lzma
from contextlib import ExitStack


def ratio(*values):
    return [i_v/values[0] for i_v in values]


with ExitStack() as stack:
    print('opening files...')
    files = [stack.enter_context(lzma.open(i_arg) if i_arg.endswith('.xz') else open(i_arg)) for i_arg in sys.argv[1:]]
    print('loading json...')
    json_data = [json.load(i_file) for i_file in files]

num_agents = [size.get('counting') for size in json_data[0].get('scenariosize')]
execution=[[i_exec.get('values')[0] for i_exec in i_json.get('time').get('execution')] for i_json in json_data]
execution_medians=[[i_median.get('50-percentile') for i_median in i_json.get('time').get('execution')] for i_json in json_data]

fig, ax = plt.subplots()
for i_exec in execution:
    ax.boxplot(i_exec)
ax.set_xticklabels(num_agents, rotation=45, fontsize=8)
ryzen = ax.plot(range(1, len(execution_medians[0])+1), execution_medians[0], color='cyan', linestyle='dashed', linewidth=1)
i7 =ax.plot(range(1, len(execution_medians[1])+1), execution_medians[1], color='blue', linestyle='solid', linewidth=1)
pi2 = ax.plot(range(1, len(execution_medians[2])+1), execution_medians[2], color='green', linestyle='dashed', linewidth=1)
pi3 = ax.plot(range(1, len(execution_medians[3])+1), execution_medians[3], color='red', linestyle='solid', linewidth=1)
ax.set_yscale('log')
ax.legend((*ryzen, *i7, *pi2, *pi3), ('Ryzen7 (1700X)', 'iMac (i7-3770)', 'PI2 (ARMv7)', 'PI3 (ARMv8)'), loc='upper left')
plt.savefig('execution_log.pdf')
plt.close()

fig, ax = plt.subplots()
for i_exec in execution:
    ax.boxplot(i_exec)
ax.set_xticklabels(num_agents, rotation=45, fontsize=8)
ryzen = ax.plot(range(1, len(execution_medians[0])+1), execution_medians[0], color='cyan', linestyle='dashed', linewidth=1)
i7 = ax.plot(range(1, len(execution_medians[1])+1), execution_medians[1], color='blue', linestyle='solid', linewidth=1)
pi2 = ax.plot(range(1, len(execution_medians[2])+1), execution_medians[2], color='green', linestyle='dashed', linewidth=1)
pi3 = ax.plot(range(1, len(execution_medians[3])+1), execution_medians[3], color='red', linestyle='solid', linewidth=1)
ax.legend((*ryzen, *i7, *pi2, *pi3), ('Ryzen7 (1700X)', 'iMac (i7-3770)', 'PI2 (ARMv7)', 'PI3 (ARMv8)'), loc='upper left')
plt.savefig('execution_lin.pdf')
plt.close()

fig, ax = plt.subplots()
execution_ratios = list(zip(*[ratio(em1, em2, em3, em4) for em1, em2, em3, em4 in zip(*execution_medians)]))
ryzen = ax.plot(range(1, len(execution_ratios[0])+1), execution_ratios[0], color='cyan', linestyle='dashed', linewidth=1)
i7 = ax.plot(range(1, len(execution_ratios[1])+1), execution_ratios[1], color='blue', linestyle='solid', linewidth=1)
pi2 = ax.plot(range(1, len(execution_ratios[2])+1), execution_ratios[2], color='green', linestyle='dashed', linewidth=1)
pi3 = ax.plot(range(1, len(execution_ratios[3])+1), execution_ratios[3], color='red', linestyle='solid', linewidth=1)
ax.set_xticklabels(num_agents, rotation=45, fontsize=8)
ax.legend((*ryzen, *i7, *pi2, *pi3), ('Ryzen7 (1700X)', 'iMac (i7-3770)', 'PI2 (ARMv7)', 'PI3 (ARMv8)'), loc='upper left')
plt.savefig('execution_ratios.pdf')
plt.close()
